import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { productsReducer } from './reducers/productReducers';
import { cartReducer } from './reducers/cartReducers';

// Initial State Value Of Whole Application
const initialState = {};

// Redux Devtool Extension
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// Creating Store
const store = createStore(
	combineReducers({
		products: productsReducer,
		cart: cartReducer,
	}),
	initialState,
	composeEnhancer(applyMiddleware(thunk))
);

export default store;
